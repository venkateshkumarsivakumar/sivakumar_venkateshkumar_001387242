/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author venkateshkumar
 */
public class CommonClass {
    
    /**
     * This method resizes the image to a size passed in params
     * 
     * @param String ImagePath
     * @param int width
     * @param int height
     * @return ImageIcon
     */
    public static ImageIcon resizeImage(String ImagePath,int width, int height)
    {
        ImageIcon MyImage = new ImageIcon(ImagePath);
        Image img = MyImage.getImage();
        Image newImg = img.getScaledInstance(width,height, Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
}
