/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author venkatesh
 */
public class VitalSigns {
    
    private double respiratoryRate;
    
    private int heartRate;
    
    private double systolicBloodPressure;
    
    private double weight;
    
    private double weightLbs;

    public double getWeightLbs() {
        return weightLbs;
    }

    public void setWeightLbs(double weightLbs) {
        this.weightLbs = weightLbs;
    }
    
    private Date time;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(double respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(double systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }
    
    @Override
    public String toString(){
        String pattern = "dd-MM-yyyy/HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String date = simpleDateFormat.format(getTime());
        return date;
    }
    
    public boolean isNormal(){
        if(getHeartRate()<70 || getHeartRate()>140 || getRespiratoryRate()<70 || getRespiratoryRate()>140 || getSystolicBloodPressure()<70 || 
                getSystolicBloodPressure()>140 || getWeight()<50 ||getWeight()>100){
            return false;
        }
        return true;  
    }
}
