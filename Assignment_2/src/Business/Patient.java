/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author venkatesh
 */
public class Patient {
    
    private VitalSignHistory vitalSignHistory;
    
    public Patient(){
       vitalSignHistory = new VitalSignHistory(); 
    }
    
    private String name;
    
    private int id;
    
    private String doctorFirstName;
    
    private String doctorLastName;
    
    private String preferedPharmacy;

    public VitalSignHistory getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(VitalSignHistory vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getPreferedPharmacy() {
        return preferedPharmacy;
    }

    public void setPreferedPharmacy(String preferedPharmacy) {
        this.preferedPharmacy = preferedPharmacy;
    }
    
    @Override
    public String toString(){
        return getName();
    }
}
