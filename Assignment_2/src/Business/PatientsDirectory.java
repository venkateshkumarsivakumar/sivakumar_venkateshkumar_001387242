/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author venka
 */
public class PatientsDirectory {
    private ArrayList<Patient> patientsDirectory;
    
    public PatientsDirectory(){
        patientsDirectory = new ArrayList<Patient>();
    }

    public ArrayList<Patient> getPatientsDirectory() {
        return patientsDirectory;
    }

    public void setPatientsDirectory(ArrayList<Patient> patientsDirectory) {
        this.patientsDirectory = patientsDirectory;
    }
    
    public Patient addPatients(){
        Patient vs = new Patient();
        patientsDirectory.add(vs);
        return vs;
    }
}
